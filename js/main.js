/* 
    Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
    сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body":
*/
const url = "https://jsonplaceholder.typicode.com/comments";
const req = new XMLHttpRequest();
const modalLoader = document.querySelector(".modal-loader");
const wrapper = document.querySelector(".wrapper");
// modalLoader.classList.add("active");
// modalLoader.classList.remove("active");

window.addEventListener("DOMContentLoaded", () => {
    //посилаємо запит на сервер для отримання даних
    req.open("GET", url);
    req.send();
    req.addEventListener("readystatechange", () => {
        if (req.readyState === 4 && req.status >= 200 && req.status < 300) {
            pagination(JSON.parse(req.responseText));

        } else if (req.readyState === 4) {
            throw new Error("Помилка у запиті!");
        }
    })
})


// відображаємо отриману інформацію на сторінці
const showCards = (arr) => {

    if (!Array.isArray(arr)) {
        throw new Error("З сервера прийшов не масив!");
    }
    arr.forEach((obj) => {

        wrapper.append(createComentCard(obj))

    })
}

//створюємо потрібну розмітку на сторніці
function createComentCard({ id, name, height, email, body, ...props }) {
    const card = document.createElement("div");
    card.classList.add("card")
    card.setAttribute("id", id)
    card.innerHTML = `
    
    <span class="card-name">${name[0].toUpperCase() + name.slice(1)}</span>
    <a class="card-email" type="email" href="mailto:${email}" class="footer__email">${email}</a>
    <div class="card-body">${body[0].toUpperCase() + body.slice(1)}</div>
    `

    return card;
}



// функціонал пагінації

function pagination(arr) {

    const ulPaginaion = document.querySelector("#pagination")
    const itemsOnPage = 10; //кількість коментарів на сторінці що показуються
    const countPagination = Math.ceil(arr.length / itemsOnPage);
    const liPaginaion = [];


    //визначення та створення кількості сторінок (меню пагінації)

    //створення кнопки назад
    const liPrev = document.createElement("li")
    liPrev.innerText = "<<<"
    liPaginaion.push(liPrev)
    ulPaginaion.appendChild(liPrev)

    // створення кнопок перемиканя сторінок
    for (let i = 1; i <= countPagination; i++) {
        const li = document.createElement("li")
        li.innerHTML = i;
        ulPaginaion.appendChild(li)
        liPaginaion.push(li)

    }

    //створення кнопки вперед
    const liNext = document.createElement("li")
    liNext.innerText = ">>>"
    liPaginaion.push(liNext)
    ulPaginaion.appendChild(liNext)

    // функціонал перемикання сторінок (при кліку)
    paginationItem(liPaginaion[1]) // яка сторінка буде показуватися при запуску

    liPaginaion.forEach(el => {

        el.addEventListener('click', function () {
            const activeLi = document.querySelector("#pagination li.active")

            if (this.innerText === ">>>") {
                if (activeLi !== liPaginaion[liPaginaion.length - 2]) {
                    paginationItem(activeLi.nextSibling);

                }

            } else if (this.innerText === "<<<") {
                if (activeLi !== liPaginaion[1]) {
                    paginationItem(activeLi.previousSibling);

                }


            } else {
                paginationItem(this)

            }

        })
    })
    function paginationItem(item) {
        const pageNum = +item.innerHTML // номер вибраної сторінки
        let start = (pageNum - 1) * itemsOnPage;
        let end = start + itemsOnPage;
        const items = arr.slice(start, end) //коментарі, що показуються, в залежності від вибраної сторінки.
        wrapper.innerHTML = "";
        showCards(items);
        const activeLi = document.querySelector("#pagination li.active")

        if (activeLi) {
            activeLi.classList.remove('active')

        }
        item.classList.add('active')
    }

}

